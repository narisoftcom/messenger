package com.narisoft.messenger.models;

import android.app.Fragment;
import android.os.Bundle;

/**
 * Created by igorkorovchenko on 16/04/2017.
 */

public class MessagesWorkerFragment extends Fragment {
    private final MessageModel mMessagesModel;

    public MessagesWorkerFragment() {
        mMessagesModel = new MessageModel();
    }

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    public MessageModel getMessageModel() {
        return mMessagesModel;
    }
}
