package com.narisoft.messenger.models;

import android.database.Observable;
import android.os.AsyncTask;
import android.util.Log;

import com.narisoft.messenger.activities.MessagesActivity;
import com.narisoft.messenger.communicator.BackendCommunicator;
import com.narisoft.messenger.communicator.CommunicatorFactory;

/**
 * Created by igorkorovchenko on 16/04/2017.
 */

public class MessageModel {

    private static final String TAG = "MessageModel";

    private final MessagesObservable mObservable = new MessagesObservable();
    private MessagesTask mMessagesTask;
    private boolean mIsWorking;

    public MessageModel() {
        Log.i(TAG, "new Instance");
    }

    public void sendMessage(final String msg, final String from, final String to) {
        if (mIsWorking) {
            return;
        }

        mObservable.notifyStarted();

        mIsWorking = true;
        mMessagesTask = new MessagesTask(msg, from, to);
        mMessagesTask.execute();
    }

    public void stopMessages() {
        if (mIsWorking) {
            mMessagesTask.cancel(true);
            mIsWorking = false;
        }
    }

    public void registerObserver(final MessagesActivity observer) {
        mObservable.registerObserver(observer);
        if (mIsWorking) {
            observer.onMessagesStarted(this);
        }
    }

    public void unregisterObserver(final MessagesActivity observer) {
        mObservable.unregisterObserver(observer);
    }

    private class MessagesTask extends AsyncTask<Void, Void, Boolean> {
        private String mMessage;
        private String mFrom;
        private String mTo;
        private boolean mResult;

        public MessagesTask(final String msg, final String from, final String to) {
            mMessage = msg;
            mFrom = from;
            mTo = to;
        }

        @Override
        protected Boolean doInBackground(final Void... params) {

            final BackendCommunicator communicator = CommunicatorFactory.createBackendCommunicator();

            try {
                return communicator.getResponse("https://svetlanadanovich.com/node.json?type=question_answers");
            } catch (InterruptedException e) {
                Log.i(TAG, "Message sending interrupted");
                return false;
            }
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mIsWorking = false;

            if (success) {
                mObservable.notifySucceeded();
            } else {
                mObservable.notifyFailed();
            }

        }
    }

    public interface Observer {
        void onMessagesStarted(MessageModel MessagesModel);

        void onMessagesSucceeded(MessageModel MessagesModel);

        void onMessagesFailed(MessageModel MessagesModel);
    }

    private class MessagesObservable extends Observable<Observer> {
        public void notifyStarted() {
            for (final Observer observer : mObservers) {
                observer.onMessagesStarted(MessageModel.this);
            }
        }

        public void notifySucceeded() {
            for (final Observer observer : mObservers) {
                observer.onMessagesSucceeded(MessageModel.this);
            }
        }

        public void notifyFailed() {
            for (final Observer observer : mObservers) {
                observer.onMessagesFailed(MessageModel.this);
            }
        }
    }

}
