package com.narisoft.messenger.communicator;

import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by igorkorovchenko on 16/04/2017.
 */

public class BackendCommunicatorStub implements BackendCommunicator {

    // String for debugging process in console "Android Monitor"
    private final String TAG = "SignInActivity";

    private String mResponseString;

    private String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    @Override
    public boolean getResponse(String urlForRequest) throws InterruptedException {

        Log.d(TAG, "getResponse");

        URL url = null;
        try {
            url = new URL(urlForRequest);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return false;
        }

        HttpURLConnection urlConnection = null;
        try {
            urlConnection = (HttpURLConnection) url.openConnection();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        try {

            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            mResponseString = convertStreamToString(in);

        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } finally {
            urlConnection.disconnect();
        }

        Log.d(TAG, String.valueOf(mResponseString));
        return true;
    }

}
