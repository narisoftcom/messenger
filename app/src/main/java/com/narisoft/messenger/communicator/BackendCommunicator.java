package com.narisoft.messenger.communicator;

/**
 * Created by igorkorovchenko on 16/04/2017.
 */

public interface BackendCommunicator {

    boolean getResponse(String urlForRequest) throws InterruptedException;

}
