package com.narisoft.messenger.communicator;

/**
 * Created by igorkorovchenko on 16/04/2017.
 */

public class CommunicatorFactory {
    public static BackendCommunicator createBackendCommunicator() {
        return new BackendCommunicatorStub();
    }
}
