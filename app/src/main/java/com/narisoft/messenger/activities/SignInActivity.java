package com.narisoft.messenger.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.narisoft.messenger.R;

/**
 * Class describes behaviar Activity for sign in action
 */
public class SignInActivity extends AppCompatActivity {

    // String for debugging process in console "Android Monitor"
    private final String TAG = "SignInActivity";

    // Fields of the Activity
    private TextView mCaptionTextView;   // Welcome caption
    private EditText mLoginEditText;     // Editable field for input an username
    private EditText mPasswordEditText;  // Editable field for input a password
    private Button mMainActionButton;    // Button for sign in with username and password

    /**
     * Default method from methods of Activity Life Cycle
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");

        setContentView(R.layout.activity_sign_in);

        // Implemetation of the Activity fields
        mCaptionTextView = (TextView) findViewById(R.id.signInCaption);
        mLoginEditText = (EditText) findViewById(R.id.signInLogin);
        mPasswordEditText = (EditText) findViewById(R.id.signInPassword);
        mMainActionButton = (Button) findViewById(R.id.signInMainAction);

        mMainActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean authorizationResult = getAuthorizationResult(
                        mLoginEditText.getText().toString(),
                        mPasswordEditText.getText().toString()
                );
                if (authorizationResult) {
                    Intent intent = getIntent();
                    intent.setClassName(getBaseContext(), MessagesActivity.class.getName());
                    startActivity(intent);
                } else {
                    new AlertDialog.Builder(SignInActivity.this)
                            .setTitle(getString(R.string.titleAlertDialogSignInFailed))
                            .setMessage(getString(R.string.messageAlertDialogSignInFailed))
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }

            }
        });

        mLoginEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b) {
                    if (!containsOnlyLettersAndNumbers(mLoginEditText.getText().toString())) {
                        mLoginEditText.setTextColor(
                                ContextCompat.getColor(getBaseContext(), R.color.colorSignInEditTextError)
                        );
                        mLoginEditText.setText(
                                getBaseContext().getResources()
                                        .getText(R.string.singnInLoginErrorWrongCharacters)
                        );
                    }
                } else {
                    mLoginEditText.setTextColor(
                            ContextCompat.getColor(getBaseContext(), R.color.colorSignInEditTextUsual)
                    );
                    mLoginEditText.setText("");
                }
            }
        });

    }

    /**
     * Method for getting a result of authorization
     * @param userName
     * @param userPassword
     * @return
     */
    private boolean getAuthorizationResult(String userName, String userPassword) {

        boolean isAuthorized = false;

        // TODO: Change to right signing in test
        if ((userName.equals("default")) && (userPassword.equals("123"))) isAuthorized = true;

        return isAuthorized;
    }

    /**
     * Method returns result of string test on special characters
     * If string doesn't contain special characters, method returns true
     * @param string
     * @return
     */
    private boolean containsOnlyLettersAndNumbers(String string) {
        if (string.matches("[0-9A-Za-z]+")) {
            return true;
        }
        return false;
    }
}
